class intervalManager {
    counter = 0;

    constructor(callback, {attempts, timeout}) {
        this.interval = setInterval(() => {
            callback({ terminateInterval: this.terminate.bind(this) });
            if (this.counter >= attempts) {
                this.terminate();
            }
            this.counter++;
        }, timeout);
    }

    terminate() {
        clearInterval(this.interval);
    }
}

function hideButtonText({terminateInterval} = {}) {
    const buttons = document.querySelectorAll('.ytd-video-primary-info-renderer [id="text"]');

    if (!!buttons.length) {
        buttons.forEach(el => el.style.display = 'none');
        terminateInterval?.();
    }
}

/**
 * Try to hide it immediately (sometimes that works)
 */
hideButtonText();

/**
 * Aggressively try to hide it in the first second
 */
new intervalManager(hideButtonText, { attempts: 40, timeout: 50 });
/**
 * Passively try to hide it once per second for the next 30 minutes (e.g. when you start on the homepage and no
 * buttons are visible yet, because a video has not been opened yet)
 */
new intervalManager(hideButtonText, { attempts: 60 * 30, timeout: 1000 });